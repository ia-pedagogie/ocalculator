package demo;

import m2103.calculator.Calculator;
import java.util.Scanner;
import m2103.calculator.CalculatorV1;
import m2103.calculator.CalculatorV2;

/**
 *
 * @author pgarric
 */
public class Main {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {

        Calculator calculator = new CalculatorV2();
        Scanner in = new Scanner(System.in);

        System.out.println(calculator.getHelp());

        while (calculator.isRunning()) {
            System.out.print("? ");
            if (!in.hasNextLine()) {
                break;
            }
            calculator.process(in.nextLine());
            String m = calculator.getMessage();
            if (m != null) {
                System.out.println(m);
            }
        }
    }
}
