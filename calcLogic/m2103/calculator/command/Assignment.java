package m2103.calculator.command;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Expression.Expression;
import m2103.calculator.Store;

/**
 * Commande qui associe un nom à une valeur
 * @author pgarric
 */
public class Assignment implements Command {

    /**
     * Le nom
     */
    String name;

    /**
     * La valeur
     */
    Expression exp;

    /**
     * Crée une commande qui associe un nom à unr valeur (variable)
     * @param name Le dit nom
     * @param exp L'expression qui contient la valeur
     */
    public Assignment(String name, Expression exp) {
        this.name = name;
        this.exp = exp;
    }

    /**
     * Execute la commande
     * @param store Le stockage de la calculatrice
     * @throws EvaluationErrorException
     */
    @Override
    public void execute(Store store) throws EvaluationErrorException {
        store.setValue(name, exp.value(store));
        store.setMessage(name + " vaut " + store.getValue(name));
        store.addToCode(getCode(store));
    }

    /**
     * Retourne le code MaP correspondant à l'assignement
     * @param store Le stockage de la calculatrice
     * @return Le dit code MaP
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        return exp.getCode(store) + "store " + name + "; ";
    }

}
