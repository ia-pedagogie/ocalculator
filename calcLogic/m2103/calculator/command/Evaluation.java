package m2103.calculator.command;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Expression.Expression;
import m2103.calculator.Store;

/**
 * La commande qui évalue simplement une expression
 * @author pgarric
 */
public class Evaluation implements Command {

    /**
     * L'expression à évaluer
     */
    Expression exp;

    /**
     * Construit l'évaluation
     * @param exp à partir de l'expression
     */
    public Evaluation(Expression exp) {
        this.exp = exp;
    }

    /**
     * Execute l'évaluation et donc stocke dans le message et le code
     * MaP de l'évaluation
     * @param store Le stockage de la calculatrice
     * @throws EvaluationErrorException
     */
    @Override
    public void execute(Store store) throws EvaluationErrorException {
        store.setMessage("Valeur" + exp.value(store));
        store.addToCode(getCode(store));
    }

    /**
     * Obtient le code MaP de l'évaluation de l'expression
     * @param store Le stockage de la calculatrice
     * @return Le dit code MaP
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        return "" + exp.getCode(store);
    }
}
