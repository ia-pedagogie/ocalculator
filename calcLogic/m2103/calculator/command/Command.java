/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m2103.calculator.command;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * L'interface qui représente une commande. Une commande peut soit
 * s'executer, soit renvoyer un code de MaP
 *
 * @author pgarric
 */
public interface Command {

    /**
     * Execute la command
     * @param store Le stockage de la calculatrice
     * @throws EvaluationErrorException
     */
    public void execute(Store store) throws EvaluationErrorException;

    /**
     * Donne le code MaP de la commande
     * @param store Le stockage de la calculatrice
     * @return Le dit code MaP
     * @throws EvaluationErrorException
     */
    public String getCode(Store store) throws EvaluationErrorException;
}
