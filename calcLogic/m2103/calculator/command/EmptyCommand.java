package m2103.calculator.command;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * La commande vide.
 * @author pgarric
 */
public class EmptyCommand implements Command {

    /**
     * Execution de la commande vide
     * @param store Le stockage de la calculatrice
     */
    @Override
    public void execute(Store store) {
        store.setMessage("");
        store.addToCode(getCode(store));
    }

    /**
     * Renvoie le code MaP de la commande vide
     * @param store Le stockage de la calculatrice
     * @return Le dit code MaP
     */
    @Override
    public String getCode(Store store) {
        return "";
    }
}
