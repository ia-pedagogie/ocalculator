package m2103.calculator.command;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * La commande pour arrêter la calculatrice
 * @author pgarric
 */
public class StopCommand implements Command {

    /**
     * Mais le calculatrice sur off
     * @param store Le stockage de la calculatrice
     */
    @Override
    public void execute(Store store) {
        store.off();
    }

    /**
     * Retourne le code MaP de l'arrêt de la calculatrice
     * @param store Le stockage de la calculatrice
     * @return Le dit code
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        return "";
    }

}
