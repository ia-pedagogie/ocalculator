package m2103.calculator;

import java.util.logging.Level;
import java.util.logging.Logger;
import m2103.calculator.Expression.Binary;
import m2103.calculator.Expression.Expression;
import m2103.calculator.command.*;
import m2103.calculator.tokens.BadTokenException;
import m2103.calculator.tokens.KeywordType;
import static m2103.calculator.tokens.KeywordType.*;
import m2103.calculator.tokens.Token;
import m2103.calculator.tokens.TokenReader;
import m2103.calculator.Expression.Number;
import m2103.calculator.Expression.Unary;
import m2103.calculator.Expression.Variable;

/**
 * Second implementation of the Calculator, with evaluation and
 * parsing at diferent moments.
 * @author pgarric
 */
public class CalculatorV2 implements Calculator {

    /**
     * Le lecteur de Tokens
     */
    private TokenReader reader = null;

    /**
     * Le stockage de la calculatrice
     */
    private final Store store = new Store();

    /**
     * Le message d'aide de cette calculatrice
     */
    private final String help = "* Calculateur de poche. Tapez :\n"
            + "-  expression\n"
            + "-  let variable = expression\n"
            + "-  un point pour arrêter\n";

    /**
     * Execute une ligne de calcul
     * @param line La dite ligne de calcul
     */
    @Override
    public void process(String line) {
        store.setMessage("");
        store.emptyCode();
        try {
            Command c = parseCommand(line);
            c.execute(store);
        } catch (SyntaxErrorException ex) {
            Logger.getLogger(CalculatorV2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EvaluationErrorException ex) {
            Logger.getLogger(CalculatorV2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Retourne le message lié à la dernière opération
     * @return le dit message
     */
    @Override
    public String getMessage() {
        return String.format(">%s\n=%s", store.getCode(), store.getMessage());
    }

    /**
     * Retourne le message d'aide de la calculatrice
     * @return Le dit message
     */
    @Override
    public String getHelp() {
        return help;
    }

    /**
     * La calculatrice est-elle sur on ?
     * @return True si elle l'est, false si non.
     */
    @Override
    public boolean isRunning() {
        return store.isOn();
    }

    /**
     * Transforme une ligne en commande
     * @param line La dite ligne
     * @return Une command correspondant à la ligne
     * @throws SyntaxErrorException
     */
    Command parseCommand(String line) throws SyntaxErrorException {//
        reader = new TokenReader(line);
        Command command;
        try {
            Token firstToken = reader.current();
            if (firstToken.is(LAST)) {
                command = new EmptyCommand();
            } else if (firstToken.is(POINT)) {
                command = new StopCommand();
                reader.next();
            } else if (firstToken.is(LET)) {
                command = parseAssignment();
            } else {
                command = parseEvaluation();
            }
            // vérifier qu'on est en fin de ligne
            Token token = reader.current();
            if (!token.is(LAST)) {
                throw new SyntaxErrorException(
                        "Fin de ligne incorrecte : "
                        + token.toString());
            }
        } catch (BadTokenException ex) {
            throw new SyntaxErrorException(ex.getMessage());
        }
        return command;
    }

    /**
     * Permet de parser une assignation d'une valeur à une variable
     * @return La command correspondante
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private Command parseAssignment() throws BadTokenException, SyntaxErrorException {
        String name;
        // lire le prochain token
        Token t = reader.next();

        // verifier que c'est un identificateur, noter son nom
        if (t.isIdentifier()) {
            name = t.getIdentifierName();
        } else {
            throw new BadTokenException("identifier was expected");
        }
        // lire le token suivant 
        if (!reader.next().is(EQUAL)) {
            throw new BadTokenException("Equal was expected");
        }
        // verifier que c'est le signe =
        // passer au token suivant
        reader.next();
        // lire une expression et noter sa valeur
        Expression exp = getExp();
        Command c = new Assignment(name, exp);
        return c;
    }

    /**
     * Permet de parser une expression d'un calcul donné dans la
     * calculatrice
     * @return L'évaluation correspondante
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private Evaluation parseEvaluation() throws BadTokenException, SyntaxErrorException {
        Expression exp = getExp();
        return new Evaluation(exp);
    }

    /**
     * Retourne l'expression qui est dans le buffer
     * @return La dite expression
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    Expression getExp() throws BadTokenException, SyntaxErrorException {
        Expression exp = getTerm();
        for (boolean looping = true; looping;) {
            Token token = reader.current();
            if (token.is(PLUS, MINUS)) {
                KeywordType operator = token.getKeywordType();
                reader.next();
                exp = new Binary(operator, exp, getTerm());
            } else {
                looping = false;
            }
        }
        return exp;
    }

    /**
     * Retourne l'expression qui correspond au terme qui se trouve
     * actuellement dans le reader
     * @return La dite expression
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private Expression getTerm() throws BadTokenException, SyntaxErrorException {
        Expression exp = getFactor();
        for (boolean looping = true; looping;) {
            Token token = reader.current();
            if (token.is(MULTIPLY, DIVIDE)) {
                KeywordType operator = token.getKeywordType();
                reader.next();
                exp = new Binary(operator, exp, getFactor());
            } else {
                looping = false;
            }
        }
        return exp;
    }

    /**
     * Retourne l'expression qui correspond au facteur qui se trouve
     * actuellement dans le reader
     * @return la dite expression
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private Expression getFactor() throws BadTokenException, SyntaxErrorException {
        Token token = reader.current();
        reader.next();
        if (token.isNumber()) {
            return new Number(token.getNumberValue());
        } else if (token.isIdentifier()) {
            return new Variable(token.getIdentifierName());
        } else if (token.is(MINUS)) {
            return new Unary(MINUS, getExp());
        } else if (token.is(LEFT_PARENTHESIS)) {
            return getExpressionEnclosedInParentheses();
        } else {
            return null;
        }
    }

    /**
     * Retourne le résultat de l'expression entre parenthèses
     * @return la dite expression
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private Expression getExpressionEnclosedInParentheses()
            throws BadTokenException, SyntaxErrorException {
        // on a déja mangé la parenthese ouvrante
        Expression exp = getExp();
        if (!reader.current().is(RIGHT_PARENTHESIS)) {
            throw new SyntaxErrorException("manque la parent. fermante");
        }
        reader.next();
        return exp;
    }
}
