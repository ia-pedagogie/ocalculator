package m2103.calculator;

/**
 *
 * @author pgarric
 */
public class divisionByZeroException extends Exception {

    public divisionByZeroException(String message) {
        super(message);
    }

}
