package m2103.calculator.tokens;

/**
 * Un jeton premettant d'identifier une variable
 * @author pgarric
 */
public class IdentifierToken implements Token {

    /**
     * Le nom de l'identifieur
     */
    private String name;

    /**
     * Construit un identifieur
     * @param name Le nom de l'identifieur
     */
    public IdentifierToken(String name) {
        this.name = name;
    }

    @Override
    public String getIdentifierName() {
        return name;
    }

    @Override
    public boolean isIdentifier() {
        return true;
    }
}
