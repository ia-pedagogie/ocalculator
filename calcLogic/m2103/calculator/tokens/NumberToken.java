package m2103.calculator.tokens;

/**
 * Positive integer numbers.
 */
public final class NumberToken implements Token {

    /**
     * La valeur du nombre
     */
    private final int value;

    /**
     * Construit un NumberToken
     * @param value à partir d'une valeur
     */
    public NumberToken(int value) {
        this.value = value;
    }

    @Override
    public boolean isNumber() {
        return true;
    }

    @Override
    public int getNumberValue() {
        return value;
    }

    @Override
    public String toString() {
        return ""+value;
    }

}
