package m2103.calculator.tokens;

import java.util.Arrays;

/**
 * Keywords : operators and reserved names.
 */
public final class KeywordToken implements Token {

    /**
     * Sentinel token for end of data
     */
    public static final KeywordToken LAST_TOKEN
            = new KeywordToken(KeywordType.LAST);

    /**
     * Le type du mot clé
     */
    private final KeywordType type;

    /**
     * Construit un KeyWordToken à partir d'un type de mot clé
     * @param type Le dit type
     */
    public KeywordToken(KeywordType type) {
        this.type = type;
    }

    @Override
    public boolean is(KeywordType... types) {
        return Arrays.asList(types).contains(this.type);
    }

    @Override
    public KeywordType getKeywordType() {
        return type;
    }

    @Override
    public String toString() {
        return "mot-clé " + type;
    }

}
