package m2103.calculator.tokens;

/**
 * Interface for tokens.
 *
 * Defines predicates about the type of token, and accessors to get
 * their specific contents.
 *
 * We use Java8 default methods in interfaces. By default, type
 * predicates return false, and accessors throw RuntimeException. They
 * are overriden in the relevant class.
 */
public interface Token {

    /**
     * Check if the token is a number. Overriden in NumberToken.
     *
     * @return true if it is a number
     */
    default public boolean isNumber() {
        return false;
    }

    /**
     * Value of a number token. Overriden in NumberToken. Should not be
     * called on non-number tokens.
     *
     * @return an intbg
     * 
     */
    default public int getNumberValue() {
        throw new RuntimeException("Erreur de programmation");
    }

    /**
     * Check if token is one of the keywords types given as parameter,
     * Overriden in KeywordToken.
     *
     * @param types
     * @return true is token is one of them
     */
    public default boolean is(KeywordType... types) {
        return false;
    }

    /**
     * Get the type of a keyword. Overriden in KeywordToken. Should not
     * be called on non-keyword tokens.
     *
     * @return a keyword type.
     */
    public default KeywordType getKeywordType() {
        throw new RuntimeException("Erreur de programmation");
    }

    /**
     * Check is token is an isIdentifier for a variable
     *
     * @return true if it is.
     */
    public default boolean isIdentifier() {
        return false;
    }

    /**
     * Get the name of an isIdentifier token. Should not be called on
     * other classes of tokens
     *
     * @return the name of the variable
     */
    public default String getIdentifierName() {
        throw new RuntimeException("Erreur de programmation");
    }
}
