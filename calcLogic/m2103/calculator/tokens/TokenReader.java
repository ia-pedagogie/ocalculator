package m2103.calculator.tokens;

import static m2103.calculator.tokens.KeywordType.*;

import static java.text.CharacterIterator.DONE;
import java.text.StringCharacterIterator;
import java.util.HashMap;
import java.util.Map;

/**
 * Get Tokens from a line.
 *
 */
public final class TokenReader {

    /**
     * La map qui associe à des caractères connus un KeyWordToken
     */
    final static Map<Character, KeywordToken> SINGLE_CHAR_SYMBOLS;

    static {
        // Note: the instructions of the "static block" are 
        // executed once, when the class is loaded

        SINGLE_CHAR_SYMBOLS = new HashMap<>();
        SINGLE_CHAR_SYMBOLS.put('+', new KeywordToken(PLUS));
        SINGLE_CHAR_SYMBOLS.put('-', new KeywordToken(MINUS));
        SINGLE_CHAR_SYMBOLS.put('*', new KeywordToken(MULTIPLY));
        SINGLE_CHAR_SYMBOLS.put('/', new KeywordToken(DIVIDE));
        SINGLE_CHAR_SYMBOLS.put('%', new KeywordToken(MODULO));
        SINGLE_CHAR_SYMBOLS.put('^', new KeywordToken(POWER));
        SINGLE_CHAR_SYMBOLS.put('.', new KeywordToken(POINT));
        SINGLE_CHAR_SYMBOLS.put('=', new KeywordToken(EQUAL));
        SINGLE_CHAR_SYMBOLS.put('(', new KeywordToken(LEFT_PARENTHESIS));
        SINGLE_CHAR_SYMBOLS.put(')', new KeywordToken(RIGHT_PARENTHESIS));
        SINGLE_CHAR_SYMBOLS.put('|', new KeywordToken(PIPE));
    }

    /**
     * La map qui associe à des mots connus un KeyWordToken
     */
    final static Map<String, KeywordToken> RESERVED_WORDS;

    static {
        RESERVED_WORDS = new HashMap<>();
        RESERVED_WORDS.put("let", new KeywordToken(KeywordType.LET));
        RESERVED_WORDS.put("sqrt", new KeywordToken(SQUAREROOT));
    }

    /**
     * Un itérateur qui passe chaque caractères de la commande
     */
    final private StringCharacterIterator charReader;

    /**
     * tokenInBuffer est-il non-null ?
     */
    private boolean hasTokenInBuffer;

    /**
     * Le token contenu dans le buffer
     */
    private Token tokenInBuffer;

    /**
     * Constructs a TokenReader for a line to be evaluated
     *
     * @param line
     */
    public TokenReader(String line) {
        charReader = new StringCharacterIterator(line);
        tokenInBuffer = null;
        hasTokenInBuffer = false;
    }

    /**
     * Accessor to the current token.
     *
     * @return the current token
     * @throws m2103.calculator.tokens.BadTokenException
     */
    public Token current() throws BadTokenException {
        if (!hasTokenInBuffer) {
            next();
        }
        return this.tokenInBuffer;
    }

    /**
     * Advances to the next token
     *
     * @return the next Token.
     * @throws m2103.calculator.tokens.BadTokenException
     */
    public Token next() throws BadTokenException {
        ignoreSpaces();
        /* the first char tells about the kind of token */
        char c = charReader.current();
        tokenInBuffer = (c == DONE) ? KeywordToken.LAST_TOKEN
                : Character.isDigit(c) ? readNumber()
                : Character.isLetter(c) ? readIdentifierOrKeyword()
                : /* otherwise ......  */ readSingleCharSymbol();
        hasTokenInBuffer = true;
        return tokenInBuffer;
    }

    /* --------------------------------------------------------
     *  Private methodes                                   
     */
    /**
     * Ignore les espace qui suive dans le charReader
     */
    private void ignoreSpaces() {
        while (Character.isSpaceChar(charReader.current())) {
            charReader.next();
        }
    }

    /**
     * Get a number from the charReader (sequence of digits)
     *
     * @return a NumberToken
     */
    private NumberToken readNumber() {
        // a sequence of decimal digits
        // iterator.current() contains the first digit
        int value = 0;
        do {
            value = 10 * value + (charReader.current() - '0');
        } while (Character.isDigit(charReader.next()));

        return new NumberToken(value);
    }

    /**
     * Get a one-char symbol from the charReader
     *
     * @return a KeywordToken
     * @throws m2103.calculator.tokens.BadTokenException
     */
    private KeywordToken readSingleCharSymbol() throws BadTokenException {
        char c = charReader.current();
        KeywordToken token = SINGLE_CHAR_SYMBOLS.get(c); // recognized?
        if (token == null) {
            throw new BadTokenException(
                    String.format("Illegal char in token \"%c\"", c));
        }
        charReader.next();
        return token;
    }

    /**
     * Read an isIdentifier from the charReader. An isIdentifier is a
     * letter followed by letters or digits. Some of them (eg. let) are
     * keywords.
     *
     * @return a KeywordToken for reserved words, or a VariableNameToken
     */
    private Token readIdentifierOrKeyword() {
        String word = "";
        while (Character.isLetterOrDigit(charReader.current())) {
            word = word.concat("" + charReader.current());
            charReader.next();
        }
        if (RESERVED_WORDS.containsKey(word)) {
            return RESERVED_WORDS.get(word);
        } else {
            return new IdentifierToken(word);
        }
    }

}
