package m2103.calculator.tokens;

/**
 * Les mot et signes réservés pour les opérations de la calculatrice
 * @author pgarric
 */
public enum KeywordType {
    // reserved keywords

    /**
     * Mot clé d'assignation de variable
     */
    LET,
    /**
     * Mot clé de la racine carré
     */
    SQUAREROOT,
    // operators

    /**
     * Mot clé du '+'
     */
    PLUS,
    /**
     * Mot clé du '-'
     */
    MINUS,
    /**
     * Mot clé du '*'
     */
    MULTIPLY,
    /**
     * Mot clé du '/'
     */
    DIVIDE,
    /**
     * Mot clé du '%'
     */
    MODULO,
    /**
     * Mot clé du '^'
     */
    POWER,
    // others

    /**
     * Mot clé du '.'
     */
    POINT,
    /**
     * Mot clé du '='
     */
    EQUAL,
    /**
     * Mot clé de '('
     */
    LEFT_PARENTHESIS,
    /**
     * Mot clé de ')'
     */
    RIGHT_PARENTHESIS,
    /**
     * Mot clé de '|'
     */
    PIPE,
    // considered as a keyword

    /**
     * Mot clé de la fin de la commande
     */
    LAST

}
