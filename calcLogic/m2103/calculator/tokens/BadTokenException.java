package m2103.calculator.tokens;

/**
 * Exception for unrecognized tokens
 */
public class BadTokenException extends Exception {

    public BadTokenException(String message) {
        super(message);
    }

}
