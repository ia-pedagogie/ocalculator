package m2103.calculator;

/**
 * Interface qui contient toutes les méthodes que l'on peut demander
 * à une calculatrice
 * @author pgarric
 */
public interface Calculator {

    /**
     * Une ligne à éxecuter
     * @param line
     */
    void process(String line);

    /**
     * Donner le message suite à l'execution de la ligne
     * @return
     */
    String getMessage();

    /**
     * Donne un message d'aide
     * @return Le dit message
     */
    String getHelp();

    /**
     * Est sur on ?
     * @return true si la calculatrice n'est pas arreter.
     */
    boolean isRunning();
}
