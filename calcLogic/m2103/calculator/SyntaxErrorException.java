package m2103.calculator;

/**
 * Exceptions to be raised when a syntax error is detected during
 * parsing.
 */
public class SyntaxErrorException extends Exception {

    public SyntaxErrorException(String message) {
        super(message);
    }
}
