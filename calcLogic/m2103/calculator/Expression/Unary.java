package m2103.calculator.Expression;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;
import m2103.calculator.tokens.KeywordType;

/**
 * Une expression qui lie un opérateur à une expression
 * @author pgarric
 */
public class Unary implements Expression {

    /**
     * L'opérateur unaire
     */
    KeywordType operateur;

    /**
     * L'expression sur lequelle s'applique l'opération
     */
    Expression expression;

    /**
     * Crée une expression unaire
     * @param operateur L'opérateur unaire
     * @param expression L'expression sur lequelle s'applique l'opération
     */
    public Unary(KeywordType operateur, Expression expression) {
        this.operateur = operateur;
        this.expression = expression;
    }

    /**
     * La valeur du résultat après l'application de l'opérateur
     * unaire sur l'expression
     * @param store Le stockage de la calculatrice
     * @return La valeur du résultat après l'application de l'opérateur
     * unaire sur l'expression
     * @throws EvaluationErrorException
     */
    @Override
    public int value(Store store) throws EvaluationErrorException {
        switch (operateur) {
            case MINUS:
                return -1 * expression.value(store);
        }
        throw new EvaluationErrorException("Signe unaire non pris en charge : "+operateur);
    }

    /**
     * Retourne le code MaP correspondant à l'application de l'opérateur
     * unaire à la dite expression
     * @param store Le stockage de la calculatrice
     * @return Le dit code
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        switch (operateur) {
            case MINUS:
                return expression.getCode(store)+"load =-1; mult;";
            default:
                throw new EvaluationErrorException("Erreur : opérateur"
                        + " unaire inconnu : "+operateur);
        }
    }

}
