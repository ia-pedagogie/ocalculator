package m2103.calculator.Expression;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * Une expression qui contient un nom de variable et récuperera
 * sa valeur dans le stockage de la calculatrice (store)
 * @author pgarric
 */
public class Variable implements Expression {

    /**
     * Le nom de la variable
     */
    String name;

    /**
     * Construit une variable
     * @param name Le nom de la variable
     */
    public Variable(String name) {
        this.name = name;
    }

    /**
     * Retourne la valeur de la variable
     * @param store Le stockage de la calculatrice
     * @return La dite valeur
     * @throws EvaluationErrorException
     */
    @Override
    public int value(Store store) throws EvaluationErrorException {
        return store.getValue(name);
    }

    /**
     * Retourne le code MaP correspondant à la récupération de la
     * valeur d'une variable dans la pile.
     * @param store Le stockage de la calculatrice
     * @return Le dit code
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        return "load " + name + "; ";
    }

}
