/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m2103.calculator.Expression;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * Une expression sera composée de d'expression sous-jacente et
 * permettra de les combinés
 * @author pgarric
 */
public interface Expression {

    /**
     * Donne la valeur correspondant à l'execution de l'expression
     *
     * @param store le stockage de la calculatrice
     * @return la dite valeur
     * @throws EvaluationErrorException
     */
    int value(Store store) throws EvaluationErrorException;

    /**
     * Retourne le code MaP de l'expression
     *
     * @param store le stockage de la calculatrice
     * @return le dit code
     * @throws EvaluationErrorException
     */
    public String getCode(Store store) throws EvaluationErrorException;

}
