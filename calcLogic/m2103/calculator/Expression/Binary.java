package m2103.calculator.Expression;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;
import m2103.calculator.tokens.KeywordType;

/**
 * Une opération liant deux expression dans un ordre préçis à un
 * opérateur
 *
 * @author pgarric
 */
public class Binary implements Expression {

    /**
     * L'opérateur qui oppose les deux expressions
     */
    KeywordType operateur;

    /**
     * L'expression de gauche et de droite
     */
    Expression expressionA,
            expressionB;

    /**
     * Crée une expression avec un opérateur et deux expressions
     *
     * @param operateur L'opérateur qui oppose les deux expressions
     * @param expressionA L'expression de gauche
     * @param expressionB L'expression de droite
     */
    public Binary(KeywordType operateur, Expression expressionA,
            Expression expressionB) {
        this.operateur = operateur;
        this.expressionA = expressionA;
        this.expressionB = expressionB;
    }

    /**
     * Retourne la valeur du résultat de cette expression
     *
     * @param store Le stockage de la calculatrice.
     * @return La valeur entière du résultat
     * @throws EvaluationErrorException
     */
    @Override
    public int value(Store store) throws EvaluationErrorException {
        switch (operateur) {
            case DIVIDE:
                return expressionA.value(store) / expressionB.value(store);
            case MULTIPLY:
                return expressionA.value(store) * expressionB.value(store);
            case MINUS:
                return expressionA.value(store) - expressionB.value(store);
            case PLUS:
                return expressionA.value(store) + expressionB.value(store);
            default:
                return Integer.MAX_VALUE;
        }
    }

    /**
     * Retourne le code en MaP
     *
     * @param store Le stockage de la calculatrice.
     * @return Le dit code
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        String op = "";
        switch (operateur) {
            case DIVIDE:
                op = "div";
                break;
            case MINUS:
                op = "minus";
                break;
            case MULTIPLY:
                op = "mul";
                break;
            case PLUS:
                op = "add";
                break;
            default:
                op = "non attribuée";
        }
        op += "; ";
        return expressionA.getCode(store) + expressionB.getCode(store) + op;
    }

}
