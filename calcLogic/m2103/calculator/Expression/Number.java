package m2103.calculator.Expression;

import m2103.calculator.EvaluationErrorException;
import m2103.calculator.Store;

/**
 * Une expression ne contenant qu'un nombre
 * @author pgarric
 */
public class Number implements Expression {

    private int number;

    /**
     * Crée un nombre
     *
     * @param number à partir de celui-ci
     */
    public Number(int number) {
        this.number = number;
    }

    /**
     * Est-ce un nombre ?
     * @return Vrai puisque nous sommes dans la classe nombre
     */
    public boolean isNumber() {
        return true;
    }

    /**
     * Retourne le nombre en question
     * @param store Le stockage de la calculatrice
     * @return Le dit nombre
     * @throws EvaluationErrorException
     */
    @Override
    public int value(Store store) throws EvaluationErrorException {
        return number;
    }

    /**
     * Retourne le code MaP du nombre
     * @param store Le stockage de la calculatrice
     * @return Le dit code
     * @throws EvaluationErrorException
     */
    @Override
    public String getCode(Store store) throws EvaluationErrorException {
        return "load =" + number + "; ";
    }

}
