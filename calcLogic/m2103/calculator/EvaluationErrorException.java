package m2103.calculator;

/**
 * Evaluation errors happen when the evaluation of an otherwise
 * syntactically correct expression fails: divide by 0, uninitialized
 * variable, etc.
 */
public class EvaluationErrorException extends Exception {

    public EvaluationErrorException(String message) {
        super(message);
    }

}
