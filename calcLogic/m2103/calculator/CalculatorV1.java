package m2103.calculator;

import java.util.HashMap;
import m2103.calculator.tokens.*;
import static m2103.calculator.tokens.KeywordType.*;

/**
 * First implementation of the Calculator, with evaluation during
 * parsing.
 */
public final class CalculatorV1 implements Calculator {

    /**
     * Le lecteur de token
     */
    private TokenReader reader = null;

    /**
     * Le stoockage de la calculatrice
     */
    private final Store store = new Store();

    /**
     * Le message d'aide
     */
    private final String help = "* Calculateur de poche. Tapez :\n"
            + "-  expression\n"
            + "-  let variable = expression\n"
            + "-  un point pour arrêter\n";

    /**
     * Déchiffrement d'une ligne
     * @param line la ligne à interpréter
     */
    @Override
    public void process(String line) {
        store.setMessage("");
        try {
            parseAndExecuteCommand(line);
        } catch (SyntaxErrorException ex) {
            store.setMessage("Erreur de syntaxe : "
                    + ex.getMessage());
        } catch (EvaluationErrorException ex) {
            store.setMessage("Erreur d'évaluation : "
                    + ex.getMessage());
        }
    }

    /**
     * La calculatrice est-elle sur on ?
     * @return True si elle l'est, faux sinon
     */
    @Override
    public boolean isRunning() {
        return store.isOn();
    }

    /**
     * Renvoie le message de la dernière ligne qui a était executée
     * @return Le dit message
     */
    @Override
    public String getMessage() {
        return store.getMessage();
    }

    /**
     * Le message d'aide
     * @return Le dit message
     */
    @Override
    public String getHelp() {
        return help;
    }

    /* -------------------------------------------------------
        private methods
     */
    /**
     * Interprète et execute la commande liée à une ligne
     * @param line la dite ligne
     * @throws SyntaxErrorException
     * @throws EvaluationErrorException
     */
    private void parseAndExecuteCommand(String line)
            throws SyntaxErrorException, EvaluationErrorException {
        try {
            reader = new TokenReader(line);
            Token firstToken = reader.current();
            if (firstToken.is(LAST)) {
                store.setMessage("");
            } else if (firstToken.is(POINT)) {
                store.off();
                reader.next();
            } else if (firstToken.is(LET)) {
                parseAndExecuteAssignment();
            } else {
                parseAndExecuteEvaluation();
            }
            // vérifier qu'on est en fin de ligne
            Token token = reader.current();
            if (!token.is(LAST)) {
                throw new SyntaxErrorException(
                        "Fin de ligne incorrecte : "
                        + token.toString());
            }
        } catch (BadTokenException ex) {
            throw new SyntaxErrorException(ex.getMessage());
        } catch (divisionByZeroException ex) {
            throw new EvaluationErrorException(ex.getMessage());
        }
    }

    /**
     * Interprète et execute le calcul d'une ligne
     * @throws SyntaxErrorException
     * @throws EvaluationErrorException
     * @throws divisionByZeroException
     */
    private void parseAndExecuteEvaluation()
            throws SyntaxErrorException, EvaluationErrorException, divisionByZeroException {
        try {
            int value = getExpressionValue();
            store.setMessage(String.format("= %d", value));
        } catch (BadTokenException ex) {
            throw new SyntaxErrorException(ex.getMessage());
        }
    }

    /**
     * Interprète et assigne le calcul d'une ligne à une variable
     * @throws BadTokenException
     * @throws EvaluationErrorException
     * @throws SyntaxErrorException
     * @throws divisionByZeroException
     */
    private void parseAndExecuteAssignment() throws BadTokenException, EvaluationErrorException, SyntaxErrorException, divisionByZeroException {
        String name;
        // lire le prochain token
        Token t = reader.next();

        // verifier que c'est un identificateur, noter son nom
        if (t.isIdentifier()) {
            name = t.getIdentifierName();
        } else {
            throw new BadTokenException("identifier was expected");
        }
        // lire le token suivant 
        if (!reader.next().is(EQUAL)) {
            throw new BadTokenException("Equal was expected");
        }
        // verifier que c'est le signe =
        // passer au token suivant
        reader.next();
        // lire une expression et noter sa valeur
        int value = getExpressionValue();
        // Construire le message de réponse
        String message = String.format("> setValue %s to %d", name, value);
        store.setValue(name, value);
        store.setMessage(message);
    }

    // --------------------------------------------------------------------
    // parsing and evaluation of expressions, terms, factors etc.
    /**
     * Retourne le résultat de l'expression contenue dans le token reader
     * @return le chiffre résultant de l'opération
     * @throws BadTokenException
     * @throws EvaluationErrorException
     * @throws SyntaxErrorException
     * @throws divisionByZeroException
     */
    private int getExpressionValue() throws
            BadTokenException,
            EvaluationErrorException,
            SyntaxErrorException,
            divisionByZeroException {

        // an expression is a sum/difference of terms
        int value = getTermValue();       // value of the first term
        for (boolean looping = true; looping;) {
            Token token = reader.current();
            if (token.is(PLUS, MINUS)) {
                KeywordType operator = token.getKeywordType();
                reader.next();
                // get next term and combine values
                value = combineValues(operator, value, getTermValue());
            } else {
                looping = false;
            }
        }
        return value;
    }

    /**
     * Combine deux valeurs pour en donner le résultat
     * @param operator l'opérateur qui sépare les deux chiffres
     * @param left chiffre de gauche
     * @param right chiffre de droite
     * @return Le résultat de l'opération
     * @throws EvaluationErrorException
     * @throws divisionByZeroException
     */
    private int combineValues(KeywordType operator, int left, int right)
            throws EvaluationErrorException, divisionByZeroException {
        int value;
        switch (operator) {
            case PLUS:
                value = left + right;
                break;
            case MINUS:
                value = left - right;
                break;
            case MULTIPLY:
                value = left * right;
                break;
            case DIVIDE:
                if (right == 0) {
                    throw new divisionByZeroException("Erreur : Division par 0 lors de :" + left + " / " + right);
                }
                value = left / right;
                break;
            case MODULO:
                value = left % right;
                break;
            case POWER:
                value = (int) Math.pow(left, right);
                break;
            default:
                throw new UnsupportedOperationException(
                        String.format("evaluation for operator '%s'",
                                operator));
        }
        return value;
    }

    /**
     * Calcul le résultat d'une multiplication ou d'une division
     * @return le résultat
     * @throws BadTokenException
     * @throws SyntaxErrorException
     * @throws EvaluationErrorException
     * @throws divisionByZeroException
     */
    private int getTermValue() throws
            BadTokenException, SyntaxErrorException, EvaluationErrorException, divisionByZeroException {

        int value = getHighestPriorityOperation();       // value of the first term
        for (boolean looping = true; looping;) {
            Token token = reader.current();
            if (token.is(MULTIPLY, DIVIDE)) {
                KeywordType operator = token.getKeywordType();
                reader.next();
                // get next term and combine values
                value = combineValues(operator, value, getTermValue());
            } else {
                looping = false;
            }
        }
        return value;
    }

    /**
     * Calcul le résultat des opérations à la plus haute prioritée
     * @return Le dit résultat
     * @throws EvaluationErrorException
     * @throws divisionByZeroException
     * @throws BadTokenException
     * @throws SyntaxErrorException
     */
    private int getHighestPriorityOperation() throws EvaluationErrorException, divisionByZeroException, BadTokenException, SyntaxErrorException {
        int value = getFactorValue();
        for (boolean looping = true; looping;) {
            Token token = reader.current();
            if (token.is(MODULO, POWER)) {
                KeywordType operator = token.getKeywordType();
                reader.next();
                value = combineValues(operator, value, getHighestPriorityOperation());
            } else {
                looping = false;
            }
        }
        return value;
    }

    /**
     * Calcul le résultat des opérations
     * @return le dit résultat
     * @throws BadTokenException
     * @throws SyntaxErrorException
     * @throws EvaluationErrorException
     * @throws divisionByZeroException
     */
    private int getFactorValue() throws
            BadTokenException, SyntaxErrorException, EvaluationErrorException, divisionByZeroException {
        Token token = reader.current();
        // starts with minus sign?
        boolean hasMinusSign = token.is(MINUS);
        if (hasMinusSign) {
            token = reader.next();
        }

        int value;
        if (token.isNumber()) {
            value = getConstantValue();
        } else if (token.isIdentifier()) {
            value = store.getValue(token.getIdentifierName());
            reader.next();
        } else if (token.is(PIPE)) {
            reader.next();
            value = Math.abs(getExpressionValue());
            if (!reader.current().is(PIPE)) {
                throw new SyntaxErrorException("Ouverture de valeur absolue, mais pas de fermeture.");
            }
            reader.next();
        } else if (token.is(SQUAREROOT)) {
            reader.next();
            value = (int) Math.sqrt(getExpressionValue());
            reader.next();
        } else if (token.is(LEFT_PARENTHESIS)) {
            reader.next();
            value = getExpressionValue();
            if (!reader.current().is(RIGHT_PARENTHESIS)) {
                throw new SyntaxErrorException("Ouverture de paranthèse, mais pas de fermeture.");
            }
            reader.next();
        } else {
            throw new SyntaxErrorException("Facteur incorrect");
        }

        // apply minus sign if requested
        if (hasMinusSign) {
            value = -value;
        }

        return value;
    }

    /**
     * Retourne la constante qui se trouve dans le reader
     * @return la dite constante
     * @throws BadTokenException
     */
    private int getConstantValue() throws BadTokenException {
        int value = reader.current().getNumberValue();
        reader.next();
        return value;
    }

}
