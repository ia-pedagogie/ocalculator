package m2103.calculator;

import java.util.HashMap;
import java.util.Map;

/**
 * Store of the calculator internals.
 *
 * Sub-component of the Calculator, so we keep it package-private.
 */
public final class Store {

    /**
     * État de la calculatrice (en marche ?)
     */
    private boolean on = true;

    /**
     * Les textes que l'on stockent qui vont être affiché lors du
     * getMessage
     */
    private String message, code;

    /**
     * Chaque variable avec sa valeur qui lui est associée
     */
    private final Map<String, Integer> values = new HashMap<>();

    /**
     * Reset the "on" indicator
     */
    public void off() {
        on = false;
    }

    /**
     * La calculatrice est-elle en fonctionnement ?
     * @return true si elle l'est, false si non
     */
    boolean isOn() {
        return on;
    }

    /**
     * Sets or modifies the value associated to a variable name.
     *
     * @param name of the variable
     * @param value
     */
    public void setValue(String name, int value) {
        values.put(name, value);
    }

    /**
     * Assigne un message
     * @param m le dit message
     */
    public void setMessage(String m) {
        message = m;
    }

    /**
     * Vide le code qui était contenu en mémoire
     */
    public void emptyCode() {
        code = "";
    }

    /**
     * Ajoute au code MaP du texte
     * @param c Le dit texte
     */
    public void addToCode(String c) {
        code += c;
    }

    /**
     * @return le code MaP qu'on lui a renseigné par les addToCode
     */
    public String getCode() {
        return code;
    }

    /**
     * @return le message du résultat de l'opération
     */
    String getMessage() {
        return message;
    }

    /**
     * Gets the value of a variable
     *
     * @param name
     * @return the value
     * @throws EvaluationErrorException if symbol is absent
     */
    public int getValue(String name) throws EvaluationErrorException {
        if (values.containsKey(name)) {
            return values.get(name);
        } else {
            throw new EvaluationErrorException("La variable " + name + " n'existe pas.");
        }

    }

}
