import unittest

from ocr_model import *


# python -m unittest test_ocr_model.py

class TestOcrModel(unittest.TestCase):

    def test_load_dataset(self):
        dataset = LabeledDataSet()

        msg = "Changement du jeu de données ?"
        self.assertEqual(dataset.dimData, (784,), msg)
        self.assertEqual(dataset.data.shape, (60493, 784), msg)
        self.assertEqual(dataset.labels.shape, (60493, 14), msg)
        self.assertEqual(dataset.test_data.shape, (10493, 784), msg)
        self.assertEqual(dataset.test_labels.shape, (10493, 14), msg)

# TODO : tester les erreurs !
# data et labels de mauvais types
# nombres inconsistants d'objets entre data et label