import os
import cv2
import keras
import matplotlib.pyplot as plt  # TODO : make this optional
import numpy as np
import sklearn  # TODO : remove this dependency, use random/numpy modules instead
import time


# TODO : make classes out of this whole bunch of code :
#   OCRModel
#   ImageProcessing ?

# TODO : change the 784-shaped-thing to the more easily understandable and adaptable 28x28 format

class LabeledDataSet:
    """ Represents a data set stored alongside with the labels. """

    def __init__(self, data: np.ndarray, labels: np.ndarray):
        """ Takes numpy arrays of same length as data and labels vectors."""
        # data and labels may have different shapes and types (uint8/float32)

        # TODO : untested
        if data.__class__ != np.ndarray or labels.__class__ != np.ndarray:
            raise TypeError("'data' and 'labels' of wrong type")

        # TODO : untested
        if data.shape[0] != labels.shape[0]:
            raise AttributeError("'data' has {} items whereas 'labels' has {}.".format(data.shape[0], labels.shape[0]))

        self.data = data
        self.labels = labels

    def data_shape(self):
        """ Returns the shape of a data item. """
        # TODO : untested
        shape = self.data.shape[1:]
        return (1,) if shape == () else shape

    def label_shape(self):
        """ Returns the shape of a label item. """
        # TODO : untested
        shape = self.labels.shape[1:]
        return (1,) if shape == () else shape

    def __len__(self):
        """ Returns the amount of data items stored with their labels. """
        # TODO : untested
        return self.data.shape[0]

    def shuffle(self):
        """ Shuffles the data in place ensuring that each item keep its label. """
        # TODO : untested
        sklearn.utils.shuffle(self.data, self.labels, random_state=0)

    def __repr__(self):
        """ String representation of a LabeledDataSet object. """
        # TODO : untested
        return "<LabeledDataSet items={} data_shape={} label_shape={}>".format(len(self), self.data_shape(),
                                                                               self.label_shape())
    def merge(self, other):
        # TODO : untested
        """ Add another data-set's labeled data to this one. """
        self.add(other.data, other.labels)

    def add(self, data, labels):
        """ Add a bunch of labeled data to this data set. """
        # TODO : untested
        if data.__class__ != np.ndarray or labels.__class__ != np.ndarray:
            raise TypeError("'data' and 'labels' of wrong type")

        # TODO : untested
        if data.shape[0] != labels.shape[0]:
            raise AttributeError("'data' has {} items whereas 'labels' has {}.".format(data.shape[0], labels.shape[0]))

        # TODO : untested
        if self.data.shape[1:] != data.shape[1:]:
            raise AttributeError(
                "current data set has shape {} whereas additional data set has shape {}.".format(
                    self.data.shape[1:],
                    data.shape[1:]))

        # TODO : untested
        if self.labels.shape[1:] != data.labels[1:]:
            raise AttributeError(
                "current labels set has shape {} whereas additional labels set has shape {}.".format(
                    self.labels.shape[1:],
                    labels.shape[1:]))

        self.data = np.concatenate((self.data, data))
        self.labels = np.concatenate((self.labels, labels))


# All those new symbols must have a directory at ../img/training_dataset/[SYMBOL]
NEW_SYMBOLS = ['plus', 'moins', 'fois', 'div']
CLASS_NAMES = list(range(10)) + NEW_SYMBOLS


def OPERATOR_SET_1():
    """ Loads our custom data set containing handwritten operators +-/x as a LabeledDataSet. """
    path = '../img/training_dataset/'
    newData = []
    newDataLabel = []

    for s in NEW_SYMBOLS:
        newDataLabel.append(CLASS_NAMES.index(s))

    def list_images(path):
        arr = []
        for imgName in os.listdir(path):
            arr.append(cv2.imread(path + "/" + imgName, 0))
        return arr

    for symbol in NEW_SYMBOLS:
        full_path = path + "/" + symbol
        newData.append(list_images(full_path))

    train_images, train_labels = [], []
    test_images, test_labels = [], []

    for i in range(len(NEW_SYMBOLS)):
        for val in newData[i][2:]:
            train_images.append(np.array(val))
            train_labels.append(newDataLabel[i])
        for val in newData[i][:2]:
            test_images.append((np.array(val)))
            test_labels.append(newDataLabel[i])

    training = LabeledDataSet(np.array(train_images), np.array(train_labels))
    test = LabeledDataSet(np.array(test_images), np.array(test_labels))

    return training, test


def MNIST():
    """ Loads the MNIST data set as LabeledDataSet objects."""
    _mnist, _mnist_test = keras.datasets.mnist.load_data()
    mnist = LabeledDataSet(*_mnist)
    mnist_test = LabeledDataSet(*_mnist_test)
    return mnist, mnist_test


def load_data_set():
    """ Loads, merges then adapts our data sets to use them with keras. """
    training, test = OPERATOR_SET_1()
    mnist, mnist_test = MNIST()

    training.merge(mnist)
    test.merge(mnist_test)

    def adapt(data_set):
        # Change from 3d-array of shape (n,28,28) to 2d-array of shape (n,784,)
        def flatten(matrix: np.ndarray) -> np.ndarray:
            return matrix.reshape(matrix.shape[0], np.prod(matrix.shape[1:]))

        data = flatten(data_set.data)

        # uint8 [0,255] --> float32 [0,1]
        data = data.astype('float32') / 255

        # Converts class vectors (integers) to binary class matrices.
        # ex : 3 -> [0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]
        labels = keras.utils.to_categorical(data_set.labels)

        new_set = LabeledDataSet(data, labels)
        new_set.shuffle()

        return new_set

    return adapt(training), adapt(test)


def create_model(training_set: LabeledDataSet, n_layers=2, layer_size=256, dropout=0.2):
    """ Creates a keras ML model with supplied parameters. """
    input_shape = training_set.data_shape()
    n_classes = training_set.label_shape()[0]

    model = keras.models.Sequential()
    model.add(keras.layers.Dense(layer_size, activation='relu', input_shape=input_shape))
    model.add(keras.layers.Dropout(dropout))
    for i in range(n_layers - 1):
        model.add(keras.layers.Dense(layer_size, activation='relu'))
        model.add(keras.layers.Dropout(dropout))
    model.add(keras.layers.Dense(n_classes, activation='softmax'))

    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def plot_history(history):
    """ Plot a model's training history. """
    plt.figure(figsize=[8, 6])
    plt.plot(history.history['loss'], 'r', linewidth=3.0)
    plt.plot(history.history['val_loss'], 'b', linewidth=3.0)
    plt.legend(['Training loss', 'Validation Loss'], fontsize=18)
    plt.xlabel('Epochs ', fontsize=16)
    plt.ylabel('Loss', fontsize=16)
    plt.title('Dropout Loss Curves', fontsize=16)

    plt.figure(figsize=[8, 6])
    plt.plot(history.history['acc'], 'r', linewidth=3.0)
    plt.plot(history.history['val_acc'], 'b', linewidth=3.0)
    plt.legend(['Training Accuracy', 'Validation Accuracy'], fontsize=18)
    plt.xlabel('Epochs ', fontsize=16)
    plt.ylabel('Accuracy', fontsize=16)
    plt.title('Dropout Accuracy Curves', fontsize=16)
    plt.show()


def test_model(model, data_set: LabeledDataSet):
    """ Tests  model's loss and accuracy on the data_set"""
    [test_loss, test_acc] = model.evaluate(data_set.data, data_set.labels)
    return test_loss, test_acc


def train(model, training_set: LabeledDataSet, testing_set: LabeledDataSet, batch_size=256, epochs=5, verbose=0):
    """ Trains a model. """
    return model.fit(training_set.data, training_set.labels, batch_size=batch_size, epochs=epochs, verbose=verbose,
                     validation_data=(testing_set.data, testing_set.labels))


def print_1d_image(image: np.ndarray, x, y):
    """ Print the 1d-array image to the console with a per-pixel threshold : non-null values are printed as *."""
    # flattened matrices vs 2d matrices ?
    for i in range(x):
        for j in range(y):
            print("*" if image[i * y + j] > 0 else " ", end="")
        print("")


def load_model():
    return keras.models.load_model('keras_model')


def main():
    training_set, testing_set = load_data_set()

    model = create_model(training_set, 2, 256, 0.2)
    history = train(model, training_set, testing_set, 256, 5)

    plot_history(history)
    print("Evaluation result on Test Data : Loss = {}, accuracy = {}".format(*test_model(model, testing_set)))
    model.save('keras_model')


def best_dropout():
    """ ML Analysis : What is the best dropout rate ? """
    # TODO : Large dropout rate: 0.6 (>0.5). In TensorFlow 2.x, dropout() uses dropout rate instead of keep_prob. Please ensure that this is intended.

    # config
    n_layers = 3
    layer_size = 28 * 4
    sub = 50  # granularité
    dropout_range = np.linspace(0, 1, sub + 1)[:-1]  # dropout 1 considered as 0 --> removed
    batch_size = 256
    epochs = 5
    repeat = 2

    loop_n = len(dropout_range) * repeat

    training_set, testing_set = load_data_set()
    results = []
    for dropout in dropout_range:
        results.append([dropout, 0, 0])
        for i in range(repeat):
            t_a = time.time()
            model = create_model(training_set, n_layers, layer_size, dropout)
            train(model, training_set, testing_set, batch_size, epochs)
            loss, acc = test_model(model, testing_set)
            results[-1][1] += loss / repeat
            results[-1][2] += acc / repeat
            loop_n -= 1
            t_b = time.time()
            delta_t = t_b - t_a
            print("Remaining time (s) : ", delta_t * loop_n)
        print("Dropout = {}  Loss = {}  Accuracy = {}".format(*results[-1]))

    print(results)

    x = [e[0] for e in results]
    loss = [e[1] for e in results]
    acc = [e[2] for e in results]
    plt.figure()
    plt.plot(x, acc, 'r', linewidth=3.0)
    plt.plot(x, loss, 'b', linewidth=3.0)
    plt.legend(['Validation Accuracy', 'Validation Loss'], fontsize=18)
    plt.xlabel('Dropout ', fontsize=16)
    # plt.ylabel('', fontsize=16)
    # plt.title('Dropout Loss Curves', fontsize=16)

    plt.show()


def main_read():
    # TODO : il y a eu bcp de modifs , update cette function !
    input_path = input("Picture to load : ")
    img = cv2.imread(input_path, 0)  # sys.argv[1]

    # As we did previously we change the data to make it understandable for the network
    img = img.reshape(1, 784)
    img = img.astype('float32')
    img /= 255
    model = load_model()
    prediction = model.predict(img)

    print(prediction)
    print("Digit: " + str(list(CLASS_NAMES[np.argmax(prediction)])))
    '''
    Here is a tricky way, but it only take the index
    of the max of prediction and search it one the CLASS_NAMES
    dictionary (an equivalent of the Map<> type in java)
    '''