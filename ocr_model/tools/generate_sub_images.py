import sys, cv2, os, numpy as np
from scipy import ndimage


def getBestShift(img):
    cy, cx = ndimage.measurements.center_of_mass(img)
    print(cy, cx)

    rows, cols = img.shape
    shiftx = np.round(cols / 2.0 - cx).astype(int)
    shifty = np.round(rows / 2.0 - cy).astype(int)

    return shiftx, shifty


def shift(img, sx, sy):
    rows, cols = img.shape
    M = np.float32([[1, 0, sx], [0, 1, sy]])
    shifted = cv2.warpAffine(img, M, (cols, rows))
    return shifted


image = sys.argv[1]
color_complete = cv2.imread(image + ".png")
gray_complete = cv2.imread(image + ".png", 0)
_, gray_complete = cv2.threshold(255 - gray_complete, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

if not os.path.exists("pro-img"):
    os.makedirs("pro-img")

cv2.imwrite("pro-img/compl.png", gray_complete)
digit_image = -np.ones(gray_complete.shape)
height, width = gray_complete.shape
predSet_ret = []

corners = []

for cropped_width in range(100, 300, 20):
    for cropped_height in range(100, 300, 20):
        for shift_x in range(0, width - cropped_width, int(cropped_width / 4)):
            for shift_y in range(0, height - cropped_height, int(cropped_height / 4)):
                gray = gray_complete[shift_y:shift_y + cropped_height, shift_x:shift_x + cropped_width]
                if np.count_nonzero(gray) <= 20:
                    continue

                if (np.sum(gray[0]) != 0) or (np.sum(gray[:, 0]) != 0) or (np.sum(gray[-1]) != 0) or (np.sum(gray[:,
                                                                                                             -1]) != 0):
                    continue

                top_left = np.array([shift_y, shift_x])
                bottom_right = np.array([shift_y + cropped_height, shift_x + cropped_width])

                while np.sum(gray[0]) == 0:
                    top_left[0] += 1
                    gray = gray[1:]

                while np.sum(gray[:, 0]) == 0:
                    top_left[1] += 1
                    gray = np.delete(gray, 0, 1)

                while np.sum(gray[-1]) == 0:
                    bottom_right[0] -= 1
                    gray = gray[:-1]

                while np.sum(gray[:, -1]) == 0:
                    bottom_right[1] -= 1
                    gray = np.delete(gray, -1, 1)

                actual_w_h = bottom_right - top_left
                if (np.count_nonzero(digit_image[top_left[0]:bottom_right[0], top_left[1]:bottom_right[1]] + 1) >
                        0.2 * actual_w_h[0] * actual_w_h[1]):
                    continue

                corner = [top_left[0], top_left[1]]
                if corners.__contains__(corner):
                    continue
                else:
                    corners.append(corner)

                print("------------------")
                print("__________________")

                rows, cols = gray.shape
                compl_dif = abs(rows - cols)
                half_Sm = int(compl_dif / 2)
                half_Big = half_Sm if half_Sm * 2 == compl_dif else half_Sm + 1
                if rows > cols:
                    gray = np.lib.pad(gray, ((0, 0), (half_Sm, half_Big)), 'constant')
                else:
                    gray = np.lib.pad(gray, ((half_Sm, half_Big), (0, 0)), 'constant')

                gray = cv2.resize(gray, (20, 20))
                gray = np.lib.pad(gray, ((4, 4), (4, 4)), 'constant')

                shiftx, shifty = getBestShift(gray)
                shifted = shift(gray, shiftx, shifty)
                gray = shifted

                cv2.imwrite("pro-img/" + image + "_" + str(shift_x) + "_" + str(shift_y) + ".png", gray)
